# Importing libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt; plt.rcdefaults()
import matplotlib
import math as mt
from scipy.sparse import csc_matrix
import seaborn as sns
import squarify
from sklearn import metrics
%matplotlib inline
%config InlineBackend.figure_format = 'retina'

# Loading in data
dataset = pd.read_csv('https://s3.amazonaws.com/dato-datasets/millionsong/10000.txt', sep='\t', names=['user_id', 'song_id', 'listen_count'], header=None)
song_data = pd.read_csv('/content/drive/MyDrive/Colab Notebooks/song_data.csv')

### Pre-processing and sparsity (emptiness / nonhomogeneity) of the data
data = pd.merge(dataset, song_data, on='song_id')
sparsity = len(data) / (data.song_id.nunique() * data.user_id.nunique() ) * 100
print('Sparsity before :', sparsity )
#print(data.shape)

# Reducing sparsity
grouped = data.groupby('user_id')
user_greater50 = grouped.filter(lambda x: len(x) > 50)
sparsity = len(user_greater50) / (user_greater50.song_id.nunique() * user_greater50.user_id.nunique() ) * 100
print('Sparsity after :', sparsity )
#print(user_greater50.shape)
users = user_greater50.user_id.nunique()
print ("Users: ", users)
songs = user_greater50.song_id.nunique()
print ("Songs: ", songs)

# Merging song title and artistname
user_greater50['song'] = user_greater50['title'] + ' - ' + user_greater50['artist_name']

### Songs
popular_songs = user_greater50.groupby(['title']).agg({'listen_count': 'count'}).reset_index()
popular_songs = popular_songs.sort_values(['listen_count', 'title'], ascending = [0,1]).head(10)

# Top 10 songs by listen count in volume graph
volume = popular_songs.listen_count
labels = popular_songs.title
squarify.plot(sizes=volume, label=labels,
              alpha=0.7)
plt.figure(figsize=(20, 15))
plt.show()

# Top 10 songs by listen count in bar graph
objects = (list(popular_songs['title']))
y_pos = np.arange(len(objects))
performance = list(popular_songs['listen_count'])
plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, objects, rotation='vertical')
plt.ylabel('Item count')
plt.title('Most popular songs')
plt.show()

### Artists
popular_artists = user_greater50.groupby(['artist_name']).agg({'listen_count': 'count'}).reset_index()
popular_artists = popular_artists.sort_values(['listen_count', 'artist_name'], ascending = [0,1]).head(10)

# Top 10 artists by listen count in volume graph
volume = popular_artists.listen_count
labels = popular_artists.artist_name
squarify.plot(sizes=volume, label=labels,
              alpha=0.7)
plt.figure(figsize=(20, 15))
plt.show()

# Top 10 artists by listen count in bar graph
objects = (list(popular_artists['artist_name']))
y_pos = np.arange(len(objects))
performance = list(popular_artists['listen_count']) 
plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, objects, rotation='vertical')
plt.ylabel('Item count')
plt.title('Most popular Artists')
plt.show()

### Recommendation by Popularity
song_grouped = user_greater50.groupby(['song']).agg({'listen_count': 'count'}).reset_index()
grouped_sum = song_grouped['listen_count'].sum()
song_grouped['percentage']  = song_grouped['listen_count'].div(grouped_sum)*100
song_grouped.sort_values(['listen_count', 'song'], ascending = [0,1])

# Splitting the data
from sklearn.model_selection import train_test_split

# Splitinto training and testing sets
X_train, X_test = train_test_split(user_greater50, 
                                                    test_size = 0.2, 
                                                    random_state = 0)
recommended_popularity = X_train.groupby(['song']).agg({'listen_count': 'count'}).reset_index()
train_popular_songs = recommended_popularity.sort_values(['listen_count', 'song'], ascending = [0,1]).head(10)
trained_grouped = pd.DataFrame(X_train.groupby('song_id')['listen_count'].count())
print(trained_grouped)

### Song example
song_example = 'SOJZXJQ12A6310F132'
songs_crosstab = pd.pivot_table(X_train, values = 'listen_count', index = 'user_id', columns = 'song_id')
#print(songs_crosstab.shape)
print(songs_crosstab.head())

# Predicted ratings of the song gte 1
predictor_song_ratings = songs_crosstab[song_example]
print(predictor_song_ratings[predictor_song_ratings>= 1])

# Similar songs related to the example
similar_songs = songs_crosstab.corrwith(predictor_song_ratings)
print(similar_songs.head())

# Similar songs and pearson coefficients (linear relationship w/ example)
corr_listened_song = pd.DataFrame(similar_songs, columns = ['pearsonR'])
corr_listened_song.dropna(inplace = True)
#print(corr_listened_song.shape)
print(corr_listened_song.head())

# Similar songs w/ pearson coefficients and listen count
predictor_corr_summary = corr_listened_song.join(trained_grouped['listen_count'])
predictor_corr_summary = predictor_corr_summary.sort_values('pearsonR', ascending = False)
print(predictor_corr_summary.head())

# Final recommendations w/ pearson coefficients of 1 excluded (unrealistic perfect correlation)
final_recommended_songs = predictor_corr_summary[predictor_corr_summary.pearsonR < 0.9999]
final_recommended_songs.sort_values('pearsonR', ascending = False)
final_recommended_songs = final_recommended_songs.reset_index()
print(final_recommended_songs.head())

# Dropping listen count as we already recommended based on popularity
song_df_one = X_train.drop(['listen_count'], axis=1)

### Recommendation by Correlation
similar_songs = pd.merge(final_recommended_songs, song_df_one.drop_duplicates(["song_id"]), on="song_id", how="left")
similar_songs = similar_songs.sort_values('pearsonR', ascending = False)
print(similar_songs.head(10))
print(X_train[X_train['song_id'] == song_example])

# Collaborative Filtering - User and Song
songs_crosstab_train = pd.pivot_table(X_train, values = 'listen_count', index = 'user_id', columns = 'song_id')
songs_crosstab_train.fillna(0,inplace=True)
songs_crosstab_test = pd.pivot_table(X_test, values = 'listen_count', index = 'user_id', columns = 'song_id')
songs_crosstab_test.fillna(0,inplace=True)

from sklearn.metrics.pairwise import pairwise_distances
user_similarity = pairwise_distances(songs_crosstab_train, metric='cosine')
song_similarity = pairwise_distances(songs_crosstab_train.T, metric='cosine')

def predict(data, similarity, mode):
    # mask: bit array indicating whether the item is rated
    mask = np.copy(data)
    mask[data > 0] = 1

    if mode == 'song_id':
        pred = data.dot(similarity) / mask.dot(np.abs(similarity))
    else:
        # User filtering
        mean = data.sum(1) / (data!=0).sum(1)
        diff = data - np.multiply(mean[:, np.newaxis], mask)
        # Old prediction rule:
        #pred = similarity.dot(data) / np.array([np.abs(similarity).sum(axis=1)]).T
        # dotting the mask excluds un-rated entried
        pred = mean[:, np.newaxis] + similarity.dot(diff) / (np.abs(similarity)).T.dot(mask)

    return pred

print ("Getting the song predictions")
song_prediction = predict(songs_crosstab_train, song_similarity, 'song_id')

print ("Getting the user predictions")
user_prediction = predict(songs_crosstab_train, user_similarity, 'user_id')

from sklearn.metrics import mean_squared_error
from math import sqrt

def evaluate(data, predictions):
    prediction = predictions[data.nonzero()].flatten()
    data = data[data.nonzero()].flatten()
    return sqrt(mean_squared_error(data, np.abs(prediction)))

def rmse(prediction, ground_truth):
    prediction = prediction[ground_truth.nonzero()].flatten() 
    ground_truth = ground_truth[ground_truth.nonzero()].flatten()
    return sqrt(mean_squared_error(prediction, ground_truth))

# RMSE based on user predictions
print(evaluate(songs_crosstab_test.to_numpy(), user_prediction))

# Setting columns equal to each other
song_prediction.columns = songs_crosstab_train.columns

# How users would rate the song example based on collaborative filtering
print(song_prediction[song_example].sort_values(ascending=False))

### Recommendation by Matrix Factorization
import scipy.sparse as sp
from scipy.sparse.linalg import svds

# Get SVD components from train matrix & choose k
u, s, vt = svds(songs_crosstab_train, k = 50)
s_diag_matrix=np.diag(s)
X_pred = np.dot(np.dot(u, s_diag_matrix), vt)
print ('User-based CF MSE: ' + str(rmse(X_pred, songs_crosstab_test.to_numpy())))

def compute_svd(urm, K):
    U, s, Vt = svds(urm, K)

    dim = (len(s), len(s))
    S = np.zeros(dim, dtype=np.float32)
    for i in range(0, len(s)):
        S[i,i] = mt.sqrt(s[i])

    U = csc_matrix(U, dtype=np.float32)
    S = csc_matrix(S, dtype=np.float32)
    Vt = csc_matrix(Vt, dtype=np.float32)
    return U, S, Vt

def compute_estimated_matrix(urm, U, S, Vt, uTest, K, MAX_PID, MAX_UID,test):
    rightTerm = S*Vt 
    max_recommendation = 250
    estimatedRatings = np.zeros(shape=(MAX_UID, MAX_PID), dtype=np.float16)
    recomendRatings = np.zeros(shape=(MAX_UID,max_recommendation ), dtype=np.float16)
    for userTest in uTest:
        prod = U[userTest, :]*rightTerm
        estimatedRatings[userTest, :] = prod.todense()
        recomendRatings[userTest, :] = (-estimatedRatings[userTest, :]).argsort()[:max_recommendation]
    return recomendRatings

# Aggregate by user and calculate sum of all songs listened by an user
dfPopularSongMetaDataMergedSum  = user_greater50[['user_id','listen_count']].groupby('user_id').sum().reset_index()
dfPopularSongMetaDataMergedSum.rename(columns={'listen_count':'total_play_count'},inplace=True)
dfPopularSongMetaDataMerged = pd.merge(user_greater50,dfPopularSongMetaDataMergedSum)

# Calculate the fractional play count
dfPopularSongMetaDataMerged['fractional_play_count'] = dfPopularSongMetaDataMerged['listen_count']/dfPopularSongMetaDataMerged['total_play_count']
print(dfPopularSongMetaDataMerged.head())

from scipy.sparse import coo_matrix
small_set = dfPopularSongMetaDataMerged

# Convert user and song IDs to integer values that can be handled by numpy matrices
user_codes = small_set.user_id.drop_duplicates().reset_index()
song_codes = small_set.song_id.drop_duplicates().reset_index()
user_codes.rename(columns={'index':'user_index'}, inplace=True)
song_codes.rename(columns={'index':'song_index'}, inplace=True)
song_codes['so_index_value'] = list(song_codes.index)
user_codes['us_index_value'] = list(user_codes.index)
small_set = pd.merge(small_set,song_codes,how='left')
small_set = pd.merge(small_set,user_codes,how='left')
mat_candidate = small_set[['us_index_value','so_index_value','fractional_play_count']]
data_array = mat_candidate.fractional_play_count.values
row_array = mat_candidate.us_index_value.values
col_array = mat_candidate.so_index_value.values

# Create a sparse matrix
data_sparse = coo_matrix((data_array, (row_array, col_array)),dtype=float)
print(data_sparse)
print(user_codes[user_codes.user_id =='f4d744784c7b3c709911690f729f5285bc240ed6'])

# Get SVD components from train matrix & choose k
K=50
utilityMatrix = data_sparse
MAX_PID = utilityMatrix.shape[1]
MAX_UID = utilityMatrix.shape[0]
U, S, Vt = compute_svd(utilityMatrix, K)
uTest = [11,21]
uTest_recommended_items = compute_estimated_matrix(utilityMatrix, U, S, Vt, uTest, K, MAX_PID, MAX_UID, True)

for  user  in  uTest:
    print("\n-------------------------------------- \
    \nRecommendation for user with id: {}\n--------------------------------------\n". format(user))
    rank_value = 1
    for i in uTest_recommended_items[user,0:10]:
        song_details = small_set[small_set.so_index_value == i].drop_duplicates('so_index_value')[['title','artist_name']]
        print("#{}: {} By {}".format(rank_value, list(song_details['title'])[0],list(song_details['artist_name'])[0]))
        rank_value+=1